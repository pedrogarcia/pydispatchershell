#include <iostream>
using std::cout;
using std::endl;
using std::cerr;
using std::ios;
using std::ifstream;
using std::ofstream;

#include <vector>
using std::vector;

#include <string>
using std::string;

#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/times.h>
#include <limits.h>
#include <sys/resource.h>

#define TRUE 1
#define FALSE 0

#define DEFAULT_TIME 20

#define FRONT_BLACK   "\033[30m"
#define FRONT_RED     "\033[31m"
#define FRONT_GREEN   "\033[32m"
#define FRONT_YELLOW  "\033[33m"
#define FRONT_BLUE    "\033[34m"
#define FRONT_MAGENTA "\033[35m"
#define FRONT_CYAN    "\033[36m"
#define FRONT_WHITE   "\033[37m"

#define BACK_BLACK   "\033[40m"
#define BACK_RED     "\033[41m"
#define BACK_GREEN   "\033[42m"
#define BACK_YELLOW  "\033[43m"
#define BACK_BLUE    "\033[44m"
#define BACK_MAGENTA "\033[45m"
#define BACK_CYAN    "\033[46m"
#define BACK_WHITE   "\033[47m"

#define NORMAL	   "\033[0m"

#define G FRONT_BLUE
#define R FRONT_RED

static int signalSIGINT = FALSE;
static int signalSIGQUIT = FALSE;
static int signalSIGHUP = FALSE;
static int signalSIGTERM = FALSE;
static int signalSIGABRT = FALSE;
static int signalSIGCONT = FALSE;
static int signalSIGTSTP = FALSE;

vector<string> instructions;


static void initInstructions(void)
{
    instructions.push_back("movl $output, %edi");
    instructions.push_back("movl %ebx, 28(%edi)");
    instructions.push_back("movl %edx, 32(%edi)");
    instructions.push_back("int $0x80");
    instructions.push_back("movl $1, %eax");
    instructions.push_back("movl $0, %ebx");
    instructions.push_back("movl %eax, value");
    instructions.push_back("pushl value(, %edi, 4)");
    instructions.push_back("add $8, $esp");
    instructions.push_back("jns loop");
    instructions.push_back("addl %ecx, %eax");
    instructions.push_back("add $8, %esp");
    instructions.push_back("movl $100, -4(%ebp)");
    instructions.push_back("cmpl $999, -4(%ebp)");
    instructions.push_back("call mmnap");
    instructions.push_back("nop");
    instructions.push_back("movl $-345, %ecx");
    instructions.push_back("movw $0xffb1, %dx");
    instructions.push_back("movl data, %ebx");
    instructions.push_back("nop");
    instructions.push_back("movw %ax, %ebx");
    instructions.push_back("xor %ebx, %ebx");
    instructions.push_back("movl $1, %eax");
    instructions.push_back("fbld data1");
    instructions.push_back("fimul data2");
    instructions.push_back("nop");
    instructions.push_back("nop");
    instructions.push_back("addb $10, %al");
    instructions.push_back("subb $10, %al");
    instructions.push_back("nop");
    instructions.push_back("imull %ebx, %ecx");
    instructions.push_back("imull $2, %edx, %eax");
    instructions.push_back("nop");
    instructions.push_back("div1 divisor");
    instructions.push_back("sall %cl, %ebx");
    instructions.push_back("adcb $0, sum(, %edi, 4)");
    instructions.push_back("test $0x00200000, %eax");
    instructions.push_back("nop");
    instructions.push_back("jnz cpuid");
    instructions.push_back("call printf");
    instructions.push_back("fstsw %ax");
    instructions.push_back("fst %st(4)");
}


static void trapSignalsFromSystem(int sig)
{	
    switch (sig) {
        case SIGINT:
            signalSIGINT = TRUE;
            break;
        case SIGQUIT:
            signalSIGQUIT = TRUE;
            break;
        case SIGHUP:
            signalSIGHUP = TRUE;
            break;
        case SIGCONT:
            signalSIGCONT = TRUE;
            break;
        case SIGTSTP:
            signalSIGTSTP = TRUE;
            break;
        case SIGABRT:
            signalSIGABRT = TRUE;
            break;
        case SIGTERM:
            signalSIGTERM = TRUE;
            break;
    }
}


void  hookupSignalTrapper(void)
{
    signal (SIGINT, trapSignalsFromSystem);
    signal (SIGQUIT, trapSignalsFromSystem);
    signal (SIGHUP, trapSignalsFromSystem);
    signal (SIGTERM, trapSignalsFromSystem);
    signal (SIGABRT, trapSignalsFromSystem);
    signal (SIGTSTP, trapSignalsFromSystem);
}


int setNumberOfInstructions(int argc, char *argv[])
{
    int cycles = argc < 2 ? DEFAULT_TIME : atoi(argv[1]);
    cycles = cycles <= 0 ? 1 : cycles;
    return cycles;
}


void printInit(pid_t pid)
{
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "START" << G << "]" << NORMAL << endl;
    cout.flush();
}


void printSIGCONT(pid_t pid)
{
    signalSIGCONT = FALSE;
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGCONT" << G << "]" << NORMAL << endl;
    cout.flush();
}


string fetchInstruction(void)
{
    srand((unsigned) time(0));
    int pos = (rand() % instructions.size());
    return instructions[pos];
}


void printINSTRUCTION(pid_t pid, int i)
{
    string ins = fetchInstruction();
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << ins << G << "]" << NORMAL << endl;
    cout.flush();
}


void printSIGINT(pid_t pid)
{
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGINT" << "]" << NORMAL << endl;
    cout.flush();
    exit(0);
}


void printSIGQUIT(pid_t pid)
{
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGQUIT" << G << "]" << NORMAL << endl;
    cout.flush();
    exit(0);
}


void printSIGHUP(pid_t pid)
{
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGHUP" << G << "]" << NORMAL << endl;
    cout.flush();
    exit(0);
}


void printSIGTSTP(pid_t pid)
{
    sigset_t mask;
    signalSIGTSTP = FALSE;
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGTSTP" << G << "]" << NORMAL << endl;
    cout.flush();
    sigemptyset (&mask);
    sigaddset (&mask, SIGTSTP);
    sigprocmask (SIG_UNBLOCK, &mask, NULL);
    signal(SIGTSTP, SIG_DFL);
    raise (SIGTSTP);
    signal(SIGTSTP, trapSignalsFromSystem);
    signalSIGCONT = TRUE;
}


void printSIGABRT(pid_t pid)
{
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGABRT" << G << "]" << NORMAL << endl;
    cout.flush();
    signal (SIGABRT, SIG_DFL);
    raise (SIGABRT);
}


void printSIGTERM(pid_t pid)
{
    cout << G << "PID[" << R << (int) pid << G << "]";
    cout << G << " OPCODE[" << R << "SIGTERM" << G << "]" << NORMAL << endl;
    cout.flush();
    exit(0);
}


bool isStillRunning(void)
{
    clock_t starttick, stoptick;
    struct tms t;
    int rc = setpriority(PRIO_PROCESS, 0, 20);
    long clktck = sysconf(_SC_CLK_TCK);
    starttick = times (&t);
    rc = sleep(1);
    stoptick = times (&t);
    return (rc == 0 || ((stoptick-starttick) > clktck/2));
}


void doInstructions(pid_t pid, int i)
{
    if (signalSIGCONT)
       printSIGCONT(pid);
    if (isStillRunning())
        printINSTRUCTION(pid, i);
    if (signalSIGINT)
        printSIGINT(pid);
    if (signalSIGQUIT)
        printSIGQUIT(pid);
    if (signalSIGHUP)
        printSIGHUP(pid);
    if (signalSIGTSTP)
        printSIGTSTP(pid);
    if (signalSIGABRT)
        printSIGABRT(pid);
    if (signalSIGTERM)
        printSIGTERM(pid);
}


int main(int argc, char *argv[])
{
    pid_t pid = getpid();
    printInit(pid);
    initInstructions();
    hookupSignalTrapper();
    int cycle = setNumberOfInstructions(argc, argv);
    for (int i = 0; i < cycle; i++) {
        doInstructions(pid, i);
    }
    exit(0);
}
