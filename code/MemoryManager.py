class mab:
    pass


def memAlloc(m, size):
    if m == None:
        m = mab()
        m.offset = 0
        m.size = size
        m.allocated = size
        m.next = None
        m.prev = None
    else:
        m.allocated = size
    return m


def memChk(mem, size):
    m = mem
    while m != None:
        if size <= m.size - m.allocated:
            return m
        else:
            m = m.next
    return None


def memFree(mem):
    m = mem
    if m == None:
        return None
    elif m.prev != None or m.next != None:
        m.allocated = 0
        m = memMerge(m)
    else:
        m = None
    return m


def memMerge(mem):
    m = mem
    ml = m.prev
    mr = m.next
    if ml != None and ml.allocated == 0:
        temp = m
        m = ml
        m.size += temp.size
        m.next = temp.next
        a = None
        if m.next != None:
            m.next.prev = m
    if mr != None and mr.allocated == 0:
        temp = mr
        m.size += temp.size
        m.next = temp.next
        if m.next != None:
            m.next.prev = m
    return m


def memSplit(mem, size):
    m = mem
    mr = mab()
    mr.offset = m.offset + size
    mr.size = m.size - size
    mr.allocated = 0
    mr.next = m.next
    if mr.next != None:
        mr.next.prev = mr
    mr.prev = m
    m.next = mr
    m.size = size
    m.allocated = size
    return m
