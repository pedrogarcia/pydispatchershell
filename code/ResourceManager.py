PRINTERS = 2
SCANNERS = 1
MODEMS = 1
CD_DRIVES = 2
MAXBUFF = 64
PRIORITYMIN = 0
PRIORITYMAX = 3
RTJOB_MEMORY = 64
MEMORY = 1024


class host_sources:
    pass


avaible_sources = host_sources()
avaible_sources.printers = PRINTERS
avaible_sources.scanners = SCANNERS
avaible_sources.modems = MODEMS
avaible_sources.cd_drives = CD_DRIVES


def rsrcAlloc(p):
    avaible_sources.printers -= p.printers
    avaible_sources.scanners -= p.scanners
    avaible_sources.modems -= p.modems
    avaible_sources.cd_drives -= p.cd_drives


def rsrcChk(p):
    if avaible_sources.printers >= p.printers and\
       avaible_sources.scanners >= p.scanners and\
       avaible_sources.modems >= p.modems and\
       avaible_sources.cd_drives >= p.cd_drives:
        return False
    else:
        return True


def rsrcFree(p):
    avaible_sources.printers += p.printers
    avaible_sources.scanners += p.scanners
    avaible_sources.modems += p.modems
    avaible_sources.cd_drives += p.cd_drives
