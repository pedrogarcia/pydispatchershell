import MemoryManager as mac
import ProcessControlBlock as pcb
import sys
import ResourceManager as rsrc


class simulation:
    pass


machine = simulation()
machine.timer = None
machine.mab = None
machine.rtmab = None
machine.inputQ = None
machine.realtimeQ = None
machine.userjobQ = None
machine.priority1Q = None
machine.priority2Q = None
machine.priority3Q = None
machine.processtable = {}


RTJOB_PRINTERMAX = 0
RTJOB_SCANNERMAX = 0
RTJOB_MODEMMAX = 0
RTJOB_CDDRIVEMAX = 0


def fill_inputQ(dispatch_list):
    for buff in dispatch_list:
        p = pcb.createNonePcb()
        p = pcb.initialisePcb(p, buff)
        machine.inputQ = pcb.enqPcb(machine.inputQ, p)


def unload_inputQ():
    while machine.inputQ != None and\
          machine.inputQ.arrivaltime <= machine.timer:
        (p, machine.inputQ) = pcb.deqPcb(machine.inputQ)
        if p.priority == rsrc.PRIORITYMIN:
            if p.printers > RTJOB_PRINTERMAX or\
               p.scanners > RTJOB_SCANNERMAX or\
               p.modems > RTJOB_MODEMMAX or\
               p.cd_drives > RTJOB_CDDRIVEMAX or\
               p.memoryusage > rsrc.RTJOB_MEMORY:
                print "Process deleted - excess memory request"
                p = None
            else:
                p.mab = mac.memAlloc(machine.rtmab, rsrc.RTJOB_MEMORY)
                machine.realtimeQ = pcb.enqPcb(machine.realtimeQ, p)
        elif p.priority <= rsrc.PRIORITYMAX:
            if p.printers > rsrc.PRINTERS or\
               p.scanners > rsrc.SCANNERS or\
               p.modems > rsrc.MODEMS or\
               p.cd_drives > rsrc.CD_DRIVES or\
               p.memoryusage > rsrc.MEMORY - rsrc.RTJOB_MEMORY:
                print "Process deleted - excess memory request"
                p = None
            else:
                machine.userjobQ = pcb.enqPcb(machine.userjobQ, p)
        else:
            print "Wrong process priority level. Chose 0, 1, 2 or 3."
            sys.exit()


def unload_userjobQ():
    while machine.userjobQ != None and\
          not rsrc.rsrcChk(machine.userjobQ):
        m = mac.memChk(machine.mab, machine.userjobQ.memoryusage)
        if m == None:
            break
        (p, machine.userjobQ) = pcb.deqPcb(machine.userjobQ)
        p.status = 'READY'
        if p.memoryusage == m.size - m.allocated:
            p.mab = mac.memAlloc(m, p.memoryusage)
        else:
            p.mab = mac.memSplit(m, p.memoryusage)
        rsrc.rsrcAlloc(p)
        if p.priority == 1:
            machine.priority1Q = pcb.enqPcb(machine.priority1Q, p)
        elif p.priority == 2:
            machine.priority2Q = pcb.enqPcb(machine.priority2Q, p)
        elif  p.priority == 3:
            machine.priority3Q = pcb.enqPcb(machine.priority3Q, p)
        else:
            print "Wrong process priority level. Chose 1, 2 or 3."
            sys.exit(-1)
