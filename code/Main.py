#!/usr/bin/env python
import Scheduler as hostd
import MemoryManager as mac
import ProcessControlBlock as pcb
import sys
import ResourceManager as rsrc
import signal
import time


QUANTUM = 1


def read_file_get_processes(filename):
    file = open(filename)
    f = []
    for line in file:
        f += [line]
    return f


def main(arc, argv):
    dispatch_list = read_file_get_processes(argv[1])
    def init_memory_structs():
        if mac.memChk(hostd.machine.mab, rsrc.MEMORY) == None:
            mab = mac.memAlloc(hostd.machine.mab, rsrc.MEMORY)
            mab = mac.memSplit(mab, rsrc.RTJOB_MEMORY)
            hostd.machine.rtmab = mab
            hostd.machine.mab = mab.next
            hostd.machine.rtmab.next = None
            hostd.machine.mab.prev = None
    def init_resources_structs():
        rsrc.avaible_sources.printers = rsrc.PRINTERS
        rsrc.avaible_sources.scanners = rsrc.SCANNERS
        rsrc.avaible_sources.modems = rsrc.MODEMS
        rsrc.avaible_sources.cd_drives = rsrc.CD_DRIVES
    init_memory_structs()
    init_resources_structs()
    hostd.fill_inputQ(dispatch_list)
    hostd.machine.timer = 0
    curr = None
    while hostd.machine.inputQ != None or\
          hostd.machine.realtimeQ != None or\
          hostd.machine.userjobQ != None or\
          hostd.machine.priority1Q != None or\
          hostd.machine.priority2Q != None or\
          hostd.machine.priority3Q != None or\
          curr != None:
        hostd.unload_inputQ()
        hostd.unload_userjobQ()
        if curr != None:
            curr.remainingcputime -= QUANTUM
            if curr.remainingcputime <= 0:
                curr = pcb.terminatePcb(curr)
                if curr.priority == rsrc.PRIORITYMIN:
                    curr.mab.allocated = 0
                else:
                    curr.mab = mac.memFree(curr.mab)
                    rsrc.rsrcFree(curr)
                curr = None
            elif curr.priority > rsrc.PRIORITYMIN and\
                 (hostd.machine.realtimeQ != None or\
                 hostd.machine.priority1Q != None or\
                 hostd.machine.priority2Q != None or\
                 hostd.machine.priority3Q != None):
                curr = pcb.suspendPcb(curr)
                if curr.priority < rsrc.PRIORITYMAX:
                    curr.priority += 1
                if curr.priority == 2:
                    hostd.machine.priority2Q = \
                        pcb.enqPcb(hostd.machine.priority2Q, curr)
                elif curr.priority == 3:
                    hostd.machine.priority3Q = \
                        pcb.enqPcb(hostd.machine.priority3Q, curr)
                curr = None
        if curr == None and\
           (hostd.machine.realtimeQ != None or\
            hostd.machine.priority1Q != None or\
            hostd.machine.priority2Q != None or\
            hostd.machine.priority3Q != None):
            if hostd.machine.realtimeQ != None:
                (p, hostd.machine.realtimeQ) = \
                    pcb.deqPcb(hostd.machine.realtimeQ)
            elif hostd.machine.priority1Q != None:
                (p, hostd.machine.priority1Q) =\
                    pcb.deqPcb(hostd.machine.priority1Q)
            elif hostd.machine.priority2Q != None:
                (p, hostd.machine.priority2Q) =\
                    pcb.deqPcb(hostd.machine.priority2Q)
            else:
                (p, hostd.machine.priority3Q) =\
                    pcb.deqPcb(hostd.machine.priority3Q)
            if p.status == 'SUSPENDED':
                p = pcb.restartPcb(p)
            else:
                p = pcb.startPcb(p)
            curr = p
        time.sleep(QUANTUM)
        hostd.machine.timer += QUANTUM
        pcb.printTable()
    hostd.machine.mab = mac.memFree(hostd.machine.mab)
    hostd.machine.rtmab = mac.memFree(hostd.machine.rtmab)
    pcb.printTable()


def signal_listener():
    for sig in range(1, signal.NSIG):
        try:
            signal.signal(sig, signal.SIG_DFL)
        except RuntimeError:
            pass


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: ./Main <processes file>"
    main(len(sys.argv), sys.argv)
    signal_listener()
