import os
import signal
import sys
import Scheduler as hostd
import colors


class pcb:
    pass


def createNonePcb():
    p = pcb()
    p.status = 'UNINITIALISED'
    p.arrivaltime = 0
    p.priority = 0
    p.remainingcputime = 0
    p.memoryusage = 0
    p.printers = 0
    p.scanners = 0
    p.modems = 0
    p.cd_drives = 0
    p.mab = None
    p.next = None
    return p


def deqPcb(head):
    if (head == None):
        return None
    else:
        p = head
        head = head.next
        p.next = None
        return (p, head)


def enqPcb(head, p):
    if (head == None):
        head = p
    else:
        curr = head
        while (curr.next != None):
            curr = curr.next
        curr.next = p
        curr.next.next = None
    return head


def initialisePcb(p, buff):
    p.status = 'INITIALISED'
    tokens = buff.split(", ")
    tokens = map(int, tokens)
    p.arrivaltime = tokens[0]
    p.priority = tokens[1]
    p.remainingcputime = tokens[2]
    p.memoryusage = tokens[3]
    p.printers = tokens[4]
    p.scanners = tokens[5]
    p.modems = tokens[6]
    p.cd_drives = tokens[7]
    return p


def setProcessColor(p):
    if p.status == 'RUNNING':
        front = colors.FRONT_GREEN
    elif p.status == 'SUSPENDED':
        front = colors.FRONT_YELLOW
    elif p.status == 'TERMINATED':
        front = colors.FRONT_RED
    else:
        front = colors.FRONT_BLUE
    return front


def updateTable(p):
    info = setProcessColor(p)
    info += "%-7s" % str(p.pid)
    info += "%-8s" % str(p.mab.offset)
    info += "%-7s" % str(p.priority)
    info += "%-6s" % str(p.remainingcputime)
    info += "%-5s" % str(p.memoryusage)
    info += "%-7s" % str(p.printers)
    info += "%-7s" % str(p.scanners)
    info += "%-8s" % str(p.modems)
    info += "%-8s" % str(p.cd_drives)
    info += "%-14s" % str(p.status)
    info += colors.DEFAULT
    hostd.machine.processtable[p.pid] = info


def printTable():
    tab = colors.CLEAR
    tab += colors.BACK_ON_CYAN + colors.FRONT_BLACK
    tab += "%-7s" % 'PID'
    tab += "%-8s" % 'OFFSET'
    tab += "%-7s" % 'PRIOR'
    tab += "%-6s" % 'TIME'
    tab += "%-5s" % 'MEM'
    tab += "%-7s" % 'PRINT'
    tab += "%-7s" % 'SCANS'
    tab += "%-8s" % 'MODEMS'
    tab += "%-8s" % 'DRIVES'
    tab += "%-14s" % 'STATUS'
    tab += "\n"
    tab += colors.DEFAULT
    for pid in hostd.machine.processtable:
        tab += hostd.machine.processtable[pid] + "\n"
    tab += colors.BACK_ON_CYAN + colors.FRONT_BLACK
    tab += "%78s" % "\n"
    tab += colors.DEFAULT
    sys.stdout.write(tab)
    sys.stdout.flush()


def printPcbInfo(p):
    updateTable(p)


def restartPcb(p):
    os.kill(p.pid, signal.SIGCONT)
    p.status = 'RUNNING'
    updateTable(p)
    return p


def startPcb(p):
    p.pid = os.fork()
    if p.pid == 0:
        p.pid = os.getpid()
        p.status = 'RUNNING'
        os.execv("./proc", ["./proc"])
    else:
        os.kill(p.pid, signal.SIGCONT)
        p.status = 'RUNNING'
    updateTable(p)
    return p


def suspendPcb(p):
    stat = os.kill(p.pid, signal.SIGTSTP)
    if stat == 0:
        print "Suspend of %d failed" % int(p.pid)
    os.waitpid(p.pid, os.WUNTRACED)
    p.status = 'SUSPENDED'
    updateTable(p)
    return p


def terminatePcb(p):
    stat = os.kill(p.pid, signal.SIGINT)
    if stat != 0:
        os.waitpid(p.pid, os.WUNTRACED)
    p.status = 'TERMINATED'
    updateTable(p)
    return p
